from django import forms
from store.models import Product

class ProductForm(forms.ModelForm):
    class Meta:
        model=Product
        fields=('category','title','description','price','image','availability')
        widget={
            'category':forms.TextInput()
        }
        
        widget={
            'title':forms.TextInput(
                attrs={'placeholder':'Enter the product name'}
            )
        }
        
        widget={
        'description':forms.Textarea()
        }
        
        widget={
            'price':forms.TextInput()
        }
        
        image=forms.ImageField(
            widget=forms.FileInput(attrs={'class':'upload logo'})
        )

        availability = forms.ChoiceField(
            choices=[('available', 'Available'), ('not_available', 'Not Available')],
            widget=forms.RadioSelect,
            label="Availability"     
        )
        