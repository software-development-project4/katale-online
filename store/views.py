from django.shortcuts import render, redirect
from store.models import Category, Product
from store.form import ProductForm

def all_products(request):
    if request.method=='POST':
        form=ProductForm(request.POST, request.FILES)
        if form. is_valid():
            product = form.save(commit=False)
            product.user = request.user  # Set the user to the current authenticated user
            product.save()
            return redirect('kataleApp:home')
            # Business registration successful
    else:
        form=ProductForm()
    return render(request, 'product.html', {'form':form})
