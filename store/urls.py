from django.urls import path
from store import views
from store.views import all_products

app_name='store'

urlpatterns=[
    path('all_products/',views.all_products, name='ProductForm'),
]