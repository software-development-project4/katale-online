from django.contrib import admin

from .models import Category, Product

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=['title','slug']
    prepopulated_fields={'slug': ('title',)}
    
@admin.register(Product)
class Productadmin(admin.ModelAdmin):
    list_display=['user','title','slug','description','price','image','availability','created_at','updated_at']
    prepopuated_fields={'slug':('title',)}


