from django.shortcuts import render, redirect
from userprofile.forms import  RegistrationForm
from django.http import HttpResponseRedirect
from django.urls import reverse
from kataleApp.models import Business
from store.models import Product

def register(request):
    if request.method == 'POST':
        form =  RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('kataleApp:user_login')
    else:
        form =RegistrationForm()
    return render(request, 'register.html', {'form': form})

def dashboard(request):
    user = request.user
    businesses = Business.objects.filter(user=user)
    products = Product.objects.filter(user=user)
    return render(request, 'dashboard.html', {'businesses': businesses, 'products': products})