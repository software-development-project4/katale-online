from django.urls import path
from userprofile import views
from userprofile.views import register, dashboard

app_name='userprofile'

urlpatterns = [
    path('register/',views.register,name='RegistrationForm'),
    path('dashboard', views.dashboard, name='dashboard'),
]
