# **KATALE ONLINE.**


## **DESCRIPTION:**
The **KATALE ONLINE** is to link business owners of different locations with their clients.
This project is to enable businesses of different locations show off goods and services they offer for sell and enable customers make bookings and orders and also call for deliveries if needed. This will help to widen market for businesses and hence increase sales and profits.

And on the side of the customers, goods and services needed by them will be readily available and there is no transports costs since goods can be ordered via this platform and delivered on their doors step..

### **USAGE:**
A user (business owner) is required to go to the website and go through these few steps;

1. Register with his/her details username and email.

2. Login into account if he/she owns one with a username and password.

3. The business owner then will fill in the business form where he will specify whether he deals in goods or services and finally upload an image/logo of his/her products he/she offers.
4. Incase a customer is interested in a given product, will only click on the product then call the seller for more details.

---
#### **AUHORS:**
| Name  | Contact |
| ---   | --------|
| Derrick |   kipaaluderrick@gmail.com |
| Allan |   @fdmallan |
| John |   @jsvico |
| Edwin |  @edwin  |
| Hadijah | @hadijah |

##### **STATUS:**
The project has just been started off.

###### **CONTRIBUTION:**
Any support towards the development of this project is welcomed.