from django import forms
from .models import Category, Business, customer, order

        
class BusinessForm(forms.ModelForm):
    class Meta:
        model=Business
        fields=('user','category','name', 'contact', 'location', 'description', 'logo')
        
        widget={
            'user':forms.TextInput()
        }
        
        widget={
            'category': forms.TextInput(
                
                attrs={
                    'placeholder':'Enter your business category',
                    'class':'form-control'
                }
            )
        }
            
        widgets={
            'name':forms.TextInput(
                
                attrs={
                    'placeholder':'Enter the name of your business',
                    'class':'form-control'
                }
            )
        }
        
        widget={
            'contact':forms.TextInput(
                
                attrs={
                    'placeholder':'Enter the your business number',
                    'class':'form-control'
                }
            )
        }
         
        widget={
            'location':forms.TextInput(
                
                attrs={
                    'placeholder':'Enter the location of your business',
                    'class':'form-control'
                }
            )
        }
        
        widget={
            'description':forms.Textarea(
                
                attrs={
                    'placeholder':'Describe your business',
                    'class':'form-control'
                    
                }
            )
        }
         
        logo=forms.ImageField(
            widget=forms.FileInput(attrs={'class':'upload logo'}),
            
        )
            
        

class customerForm(forms.ModelForm):
    class Meta:
        model=customer
        fields=('First_name', 'last_name', 'email', 'contact','location')
        
class orderForm(forms.ModelForm):
    class Meta:
        model=order
        fields=('customer','products','total_amount', 'delivery_choices','payment_status')
        widget={
            'customer':forms.TextInput(
                attrs={
                    'placeholder':'choose your name',
                    'class':'form-control'
                    }
                )
        }
         
        widget={
            'products':forms.TextInput(
                attrs={
                    'placeholder':'choose produt name',
                    'class':'form-control'
                    }
                )
            }   
        
        widget={
            'total_amount':forms.TextInput(
                attrs={
                    'placeholder':'enter total amount',
                    'class':'form-control'
                }
            )
        }
        
        widget={
            
            'delivery_choice':forms.TextInput(),
        }


class contact(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    myself = forms.BooleanField(required=False)
    
   
class LoginForm(forms.Form):
    username = forms.CharField(label="Username")
    password = forms.CharField(label="Password", widget=forms.PasswordInput)
 
        