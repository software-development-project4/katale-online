from django.apps import AppConfig


class KataleappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kataleApp'
