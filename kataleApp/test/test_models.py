from django.test import TestCase
from kataleApp.models import Business, Category

class BusinessModelTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a Category object for ForeignKey reference
        cls.category = Category.objects.create(name="Test Category" )

    def test_business_creation(self):
        # Create a Business object
        business = Business.objects.create(
            category=self.category,
            name="Test Business",
            contact="Test Contact",
            location="Test Location",
            description="Test Description",
            logo="test_image.jpg",  # You can replace this with an actual image path
            register=True
        )

        # Checking if the business was created successfully
        self.assertEqual(business.name, "Test Business")
        self.assertEqual(business.contact, "Test Contact")
        self.assertEqual(business.location, "Test Location")
        self.assertEqual(business.description, "Test Description")
        self.assertEqual(business.register, True)

    def test_string_representation(self):
        # Creating a Business object
        business = Business.objects.create(
            category=self.category,
            name="Test Business",
            contact="Test Contact",
            location="Test Location",
            description="Test Description",
            logo="media/images/logo.jpg",
            register=True
        )

        # Checking if the string representation is correct
        self.assertEqual(str(business), "Test Business")