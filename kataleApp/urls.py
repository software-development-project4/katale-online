from django.urls import path

from kataleApp import views
from kataleApp.views import register_business, landing_page, search_results, order_details, product_details
from django.contrib.auth import views as auth_views

app_name= 'kataleApp'

urlpatterns=[
    path('', views.home, name='home'),
    path('product_details/<int:id>/', views.product_details, name='product_details'),
    path('orders', views.all_orders, name='all_orders'),
    path('accounts/login/',auth_views.LoginView.as_view(template_name='registration/user_login.html'), name='user_login'),
    path('register_business/',register_business, name='BusinessForm'),
    path('logout/',views.logout, name='logout'),
    path('landing_page/', views.landing_page, name='landing_page'),
    path('about_us/',views.about_us,name='about_us'),
    path('search/', views.search_results, name='search_results'), 
    path('order_details', views.order_details, name='order_details') 
]
