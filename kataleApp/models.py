from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from store.models import Product

class Category(models.Model):
    name=models.CharField(max_length=255, db_index=True)
    slug=models.SlugField(max_length=255, unique=True)
    
    class Meta:
        verbose_name_plural= 'categories'
    
    def __str__(self):
        return self.name

class Business(models.Model):
    user=models.ForeignKey(User, related_name='Businesses', on_delete=models.CASCADE, default=None)
    category=models.ForeignKey(Category, related_name='Businesses', on_delete=models.CASCADE)
    name=models.CharField(max_length=255)
    contact=models.CharField(max_length=255)
    location=models.CharField(max_length=255)
    description=models.TextField(blank=True)
    logo=models.ImageField(upload_to='images/')
    register=models.BooleanField(default=False)
    
    class Meta:
        verbose_name_plural= 'Businesses'
        ordering=('name',)
    
    def __str__(self):
        return self.name

class customer(models.Model):
    First_name=models.CharField(max_length=200)
    last_name=models.CharField(max_length=200)
    email=models.EmailField()
    contact=models.CharField(max_length=10)
    location=models.CharField(max_length=20, blank=True, default='Kampala')

        
    def __str__(self):
        return self.First_name
    
class order(models.Model):
    customer=models.ForeignKey(customer, on_delete=models.CASCADE)
    products=models.ManyToManyField(Product)
    total_amount=models.IntegerField(default=1)
    DELIVERY=[('delivery', 'Delivery'), ('pickup','Pickup')]
    delivery_choices=models.CharField(max_length=10, choices=DELIVERY, default='Delivery')
    PAYMENT=[('cleared','Cleared'), ('pending','Pending')]
    payment_status=models.CharField(max_length=10, choices=PAYMENT, default='Pending')
    order_date=models.DateTimeField(auto_now_add=True)
    
