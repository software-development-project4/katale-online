from django.contrib import admin

# Register your models here.
from .forms import BusinessForm, customerForm, orderForm
from .models import Category, Business, customer, order
from store.models import Product

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=['name','slug']
    prepopulated_fields={'slug': ('name',)}
    
@admin.register(Business)
class BusinessAdmin(admin.ModelAdmin):
    form=BusinessForm
    list_display=['name', 'contact', 'location','description','logo']
    list_editable=['description',]

@admin.register(customer)
class customerAdmin(admin.ModelAdmin):
    form=customerForm
    list_display=['First_name', 'last_name', 'email', 'contact','location']
    
@admin.register(order)
class OrderAdmin(admin.ModelAdmin):
    form=orderForm
    list_display = ['customer','product_titles','total_amount', 'delivery_choices', 'order_date']
    
    def product_titles(self, obj):
        products = obj.products.all()
        return ", ".join([product.title for product in products])  # Joining titles into a string
    product_titles.short_description = 'Ordered Products'
    
    def delivery_choices(self, obj):
        return obj.delivery_choices()
    delivery_choices.short_description = 'Delivery/Pickup'
    
    def payment_status(self,obj):
        return obj.payment_status()
    payment_status.short_description = 'Cleared/Pending' 
    