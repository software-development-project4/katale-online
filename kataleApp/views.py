from django.shortcuts import render, redirect, get_object_or_404
from .forms import BusinessForm, LoginForm, orderForm, customerForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from store.models import Product
from django.contrib.auth.models import User
from django.http import HttpResponseNotFound

from .models import Category, Business, customer, order

def home(request):
    # Retrieve all products
    all_products = Product.objects.all()
# Group products by category
    grouped_products = {}
    for product in all_products:
        category_name = product.category.title
        if category_name not in grouped_products:
            grouped_products[category_name] = []
        grouped_products[category_name].append(product)

    context = {
        'grouped_products': grouped_products,
    }
    return render(request, 'kataleApp/home.html', context)

def product_details(request,id):
    product=get_object_or_404(Product,id=id )
    business = product.user.Businesses.first() 
    business_contact =business.contact 
    context = {
        'product': product,
        'business_contact': business_contact
    }
    return render(request, 'product_details.html', context)

def all_orders(request):
    product_names = []
    form=orderForm()
    if request.method == 'POST':
        form = orderForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            order.customer = form.cleaned_data['customer'] # Set the customer name to the current order
            # Accessing selected products and saving their names
            product_names = [product.title for product in form.cleaned_data['products']]
            order.save()
            order.products.add(*form.cleaned_data['products'])  # Adding selected products to the order
            print("Order made with products:", product_names)
            return redirect('kataleApp:order_details')  # order made successfully
        
    else:
        form = orderForm()
    return render(request, 'order.html', {'form': form, 'product_names': product_names})




def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                user_login(request, user)
                # Redirect to a success page or some other page
                return redirect('store:ProductForm')  
            else:
                # Invalid login
                error_message = "Invalid username or password."
                return render(request, 'user_login.html', {'form':form})
    else:
        form = LoginForm()
    return render(request, 'user_login.html', {'form': form})

    
@login_required
def landing_page(request):
    if request.user.is_authenticated:
        # Filter businesses based on the name of the user who registered them
        username = User.objects.filter()
        if username.exists():
            return redirect('userprofile:dashboard')  # Redirect to product upload form if the user has registered any business
    # If the user is not authenticated or has not registered any businesses, redirect to the business registration form
    return redirect('kataleApp:BusinessForm')

def register_business(request):
    if request.method == 'POST':
        form=BusinessForm(request.POST, request.FILES)
        if form.is_valid():
            business = form.save(commit=False)
            business.user = request.user  # Set the user
            business.save()
            return redirect('store:ProductForm')  # Redirect to product upload form
            # Business registration successful
    else:
        form = BusinessForm()
    return render(request, 'register_business.html', {'form': form})

    
def logout(request):
    logout(request)
    return redirect('katale/home.html')
    
def about_us(request):
    return render(request,'about_us.html')


def search_results(request):
    query = request.GET.get('query')
    if query:
        # Performing search based on the query
        search_results = Product.objects.filter(title__icontains=query)

        # Checking if any products are found in the search results
        if search_results.exists():
            # Redirecting to the first product's detail 
            first_product = search_results.first()
            if first_product.availability: 
                # Redirecting to product detail page if product is available
                return redirect('kataleApp:product_details', id=first_product.id)
            else:
                return HttpResponseNotFound("Product not available")
        else:
            search_results = None
    else:
        search_results = None

    return render(request, 'search_results.html', {'search_results': search_results, 'query': query})

def order_details(request):
    # Getting the customer's name from the request
    customer_name = request.POST.get('customer')

    # Filtering orders based on the customer's name
    orders = order.objects.filter(customer=customer_name)

    # Calculating the total price for each order
    for order_item in orders:
        total_price = 0
        for product in order_item.products.all():
            total_price += order_item.quantity * product.price
        order_item.total_price = total_price

    context = {
        'orders': orders
    }
    return render(request, 'order_details.html', context)

def product_detail(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    similar_products = Product.objects.filter(category=product.category).exclude(pk=product_id)[:3]
    return render(request, 'product_detail.html', {'product': product, 'similar_products': similar_products})


    
    